#!/bin/bash

step=10
volume_max=150

amixer_up(){
	amixer_mute "on"
	amixer set Master $step%+
}
amixer_down(){
	amixer_mute "on"
	amixer set Master $step%-
}
amixer_mute(){
	if [ -n "$1" ]; then
		amixer set Master $1
	else
		amixer set Master toggle
	fi
}

pactl_up(){
	pactl_mute "false"
	volume=$(pactl list sinks | grep '^[[:space:]]Громкость:' | head -n $(( $SINK + 1 )) | tail -n 1 | sed -e 's,.* \([0-9][0-9]*\)%.*,\1,')
	echo $volume
	if [ "$volume" -lt "$volume_max" ]; then   # меньше
		echo "q"
            pactl -- set-sink-volume @DEFAULT_SINK@ +$step%
        else
	    pactl -- set-sink-volume @DEFAULT_SINK@ $volume_max%
	fi
}
pactl_down(){
	pactl_mute "false"
	pactl -- set-sink-volume @DEFAULT_SINK@ -$step%
}
pactl_mute(){
	if [ -n "$1" ]; then
		pactl set-sink-mute @DEFAULT_SINK@ $1
	else
		pactl set-sink-mute @DEFAULT_SINK@ toggle
	fi
}

amixer=$(command -v amixer)
pactl=$(command -v pactl)
mode=""
if [ "$pactl" ]; then
	mode="pactl"
elif [ "$amixer" ]; then
	mode="amixer"
fi

if [ "$mode" ] && ( [[ $1 == "up" ]] || [[ $1 == "down" ]] || [[ $1 == "mute" ]] ); then
	$mode"_"$1
fi


