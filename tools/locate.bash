#!/bin/bash

HISTORY_CAPACITY=30
HISTORY_FILE=~/.locatesearch_history

#notify-send "Update db... Please wait" -i /usr/share/icons/oxygen/base/64x64/apps/kmag.png
#sudo updatedb &

MESG="""<b>.пробел</b>         - включить скрытые файлы и папки
<b>Ctrl+Enter</b> - поиск введёного текста, игнорируя историю"""
name=$( echo "$(cat $HISTORY_FILE)" | rofi -dmenu -p "Find" -mesg "${MESG}" -width 70) || exit


$(sed -i "/^$name$/d" $HISTORY_FILE)
hist=$(cat $HISTORY_FILE)
echo "$name" > $HISTORY_FILE
echo "$hist" >> $HISTORY_FILE

line_hist_count=$(cat $HISTORY_FILE | wc -l)
if [ "$line_hist_count" -ge "$HISTORY_CAPACITY" ]; then
	sed -i '$d' $HISTORY_FILE
fi

#wait # ожидаем окончания updatedb

if [ "$(echo "$name" | grep -v "^\." )" ]; then # в одну регулярку объеденить не смог, почемуто \< (начало слова) ломает минус
   list="$( locate -A -i $name | grep ~/ | grep -v "/\."  )"
else
   name=$(echo "$name" | sed 's/^.\>//' | sed 's/[[:space:]].\>//') # в одну регулярку объеденить не смог
   list="$( locate -A -i $name | grep ~/ |grep -v .git  )"
fi

if [ -z "$list" ]; then
    rofi -e "~~~  НИЧЕГО НЕ НАЙДЕНО  ~~~"
    exit
fi


MESG="""<b>Enter</b> - открыть в приложении по умолчанию. 
<b>Alt+e</b> - открыть папку с файлом в файловом менеджере."""
file=$(printf '%s\n' "${list[@]}" | rofi -dmenu -i -p "$name" -kb-custom-1 Alt+e -mesg "${MESG}" -width 70)
retCodeRofi=$?

if [ -z "$file" ]; then
    rofi -e "~~~  НИЧЕГО НЕ НАЙДЕНО  ~~~"
    exit
fi

if [ "$retCodeRofi" -eq 10 ]; then
    xdg-open "${file%/*}"
else
    xdg-open "$file"
fi
