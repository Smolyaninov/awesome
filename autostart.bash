#!/bin/bash


#ROOT_PATH=$(cd $(dirname $0) && pwd);
#RUN_ONCE="$ROOT_PATH/run_once.bash"

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

#бук с arch
if [[ $(hostname -s) = "mo-mini" ]]; then
    # keyboard lyouts
    setxkbmap -layout 'us,ru' -variant -option 'grp:toggle,grp_led:caps, terminate:ctrl_alt_bksp, caps:escape' &
    # package manager
    run pamac-tray
    # network manager
    run nm-applet
fi

#gui autorisation
if [[ $(hostname -s) = "mo-mini" ]]; then
   run  /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
else 
   run  /usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1
fi
#disable monitor shutdown
exec xset s off &
exec xset dpms 0 0 0 &
#export PRINTECR=HP_LaserJet_1022

#exec $RUN_ONCE xbindkeys&
#run goldendict -- En- Ru dictionary
#run artha -- En-En trhesarius
run xfce4-clipman # менеджер буфера обмена
#run redshift-gtk -- xgamma и xrandr --gamma приводят к жутким тормазам, но в gnome всё работает
